﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoldManager : MonoBehaviour
{
    private bool isPress;
    [SerializeField] private BoxController boxController;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            isPress = true;
            boxController.StartBottlesMovement();
        }

        if (Input.GetMouseButtonUp(0))
        {
            isPress = false;
            boxController.StopBottlesMovement();
        }
    }
}