﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaleStandController : MonoBehaviour
{
    private List<Transform> bottlePositions;
    private List<Transform> shelvedBottles;

    private int cursor = 0;
    private Vector3 averagePoint;

    [SerializeField] private GameObject particlePrefab;


    private bool canCheckFull = false;

    public Vector3 AveragePoint
    {
        get { return averagePoint; }
    }

    public List<Transform> ShelvedBottles => shelvedBottles;


    // Start is called before the first frame update
    void Start()
    {
        shelvedBottles = new List<Transform>();
        Vector3 totalPositions = Vector3.zero;
        bottlePositions = new List<Transform>();
        for (int i = 0; i < transform.childCount; i++)
        {
            bottlePositions.Add(transform.GetChild(i));
            transform.GetChild(i).gameObject.SetActive(false);
            totalPositions += transform.GetChild(i).position;
        }

        averagePoint = totalPositions / bottlePositions.Count;
    }

    public Transform GetNextBottleTransform()
    {
        cursor++;
        if (cursor - 1 < bottlePositions.Count)
            return bottlePositions[cursor - 1];
        else
            return null;
    }

    public bool IsStandFull()
    {
        if (cursor == bottlePositions.Count - 1)
        {
            canCheckFull = true;
            StartCoroutine(CheckIsStandFull());
        }

        if (cursor == bottlePositions.Count)
        {
            canCheckFull = false;
            StopCoroutine(CheckIsStandFull());
            return true;
        }
        else
        {
            return false;
        }
    }

    private IEnumerator CheckIsStandFull()
    {
        float elapsedTime = 0;
        while (elapsedTime <= 2)
        {
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        if (elapsedTime >= 2 && canCheckFull)
        {
            StartCoroutine(WiningCoroutine());
            Debug.Log("WiN");
        }
    }

    private IEnumerator WiningCoroutine()
    {
        for (int i = 0; i < shelvedBottles.Count; i++)
        {
            Instantiate(particlePrefab, shelvedBottles[i].position - Vector3.forward, Quaternion.identity);
            yield return new WaitForSeconds(0.1f);
        }
    }
}