﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Packages.Rider.Editor.UnitTesting;
using UnityEngine;

public class BoxController : MonoBehaviour
{
    private List<Transform> bottles;
    private bool canBottlesMove = false;


    [SerializeField] private SaleStandController standController;

    [SerializeField] private float holdDelay = 0.4f;
    [SerializeField] private float holdTime = 0.2f;
    [SerializeField] private float duration = 1;

    private int cursor = 0;

    private bool IsMovementActive = true;

    [SerializeField] private AnimationCurve animationCurve;

    // Start is called before the first frame update
    void Start()
    {
        bottles = new List<Transform>();
        for (int i = 0; i < transform.childCount; i++)
        {
            bottles.Add(transform.GetChild(i));
        }
    }

    public void StartBottlesMovement()
    {
        canBottlesMove = true;
        StartCoroutine(BottleMovementCoroutine());
    }

    public void StopBottlesMovement()
    {
        canBottlesMove = false;
        StopCoroutine(BottleMovementCoroutine());
    }

    private IEnumerator BottleMovementCoroutine()
    {
        float elapsedHoldStartTime = 0;
        //float elapsedDelayStartTime = 0;

        //Debug.Log("SayacBasladi");

        while (canBottlesMove && IsMovementActive)
        {
            //  Debug.Log(Time.time);

            if (elapsedHoldStartTime > holdTime)
            {
                // Debug.Log(Time.time);
                Transform bottle = GetNextBottle();

                if (!standController.IsStandFull())
                {
                    Transform targetTransform = standController.GetNextBottleTransform();
                    //StartCoroutine(MovePosition(bottle, targetTransform.position, duration));
                    bottle.DOJump(targetTransform.position, 1.5f, 1, duration).SetEase(animationCurve);
                    bottle.DOLocalRotate(bottle.localEulerAngles + (Vector3.right * 360), duration,
                        RotateMode.FastBeyond360);
                }
                else
                {
                    BreakBottles();
                    bottle.DOJump(standController.AveragePoint, 1.5f, 1, duration).SetEase(animationCurve);
                    bottle.DOLocalRotate(bottle.localEulerAngles +
                                         (Vector3.right * Random.Range(10, 50)) +
                                         Vector3.up * Random.Range(10, 50) +
                                         Vector3.forward * Random.Range(10, 50), duration).OnComplete(() =>
                    {
                        bottle.GetComponent<Rigidbody>().isKinematic = false;
                    });
                    IsMovementActive = false;
                }

                bottle.DOScale(bottle.localScale * 2, duration);


                standController.ShelvedBottles.Add(bottle);

                yield return new WaitForSeconds(holdDelay);
            }
            else
            {
                elapsedHoldStartTime += Time.deltaTime;
            }

            yield return null;
        }

        //Debug.Log("SayacSonlandi");
    }

    private IEnumerator MovePosition(Transform targetTransform, Vector3 targetPoint, float duration)
    {
        Vector3 startPoint = targetTransform.position;
        float elapsedTime = 0;

        while (elapsedTime <= duration)
        {
            targetTransform.position = Vector3.Lerp(startPoint, targetPoint, elapsedTime / duration);
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        targetTransform.position = targetPoint;

        yield return null;
    }

    [ContextMenu("TestTest")]
    public void Test()
    {
        float ratio = Mathf.Lerp(10, 20, 0.2345f);
        Vector3 calculatedRatio = Vector3.Lerp(new Vector3(1, 0.5f, 0), new Vector3(5, 2, 0), 0.2345f);

        Debug.Log("floatRation " + ratio);
        Debug.Log("Vector3Ratio " + calculatedRatio);
    }

    [ContextMenu("BreakBottles")]
    private void BreakBottles()
    {
        for (int i = 0; i < standController.ShelvedBottles.Count; i++)
        {
            standController.ShelvedBottles[i].GetComponent<Rigidbody>().isKinematic = false;
        }
    }

    private Transform GetNextBottle()
    {
        cursor++;
        if (cursor - 1 < bottles.Count)
            return bottles[cursor - 1];
        else return null;
    }
}