﻿using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

public class DragControl : MonoBehaviour
{
    [SerializeField] private Camera targetCam;
    private Vector3 mOffset;
    private float mZCoord;
    [SerializeField] private LayerMask layerMask;
    private bool isDrag = false;
    private Transform selectedTransform;
    [SerializeField] private Vector3 holdOffset;
    
    
    public bool isActive = false;
    //private DragProduct[] dragProducts;

    public UnityAction OnTap;

    private Vector3 selectedProductFirstPosition;
    private MyProduct selectedProduct;

    private void Update()
    {
        if (!isActive)
            return;

        #region Mobile Inputs

        if (Input.touches.Length > 0)
        {
            if (Input.touches[0].phase == TouchPhase.Began)
            {
                isDrag = true;

                RaycastHit hit;
                Ray ray = targetCam.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.transform.TryGetComponent(out CardboardBox cardboardBox))
                    {
                        selectedProduct = cardboardBox.GetProduct(hit.point);

                        selectedTransform = selectedProduct.ProductTransform;
                        selectedProductFirstPosition = selectedTransform.position;

                        var tempPosition = selectedTransform.localPosition;
                        tempPosition.z = 8;
                        selectedTransform.localPosition = tempPosition;
                        MyOnMouseDown(selectedTransform);
                    }

                    // Debug.Log(hit.transform);
                    // Do something with the object that was hit by the raycast.
                }
            }
            else if (Input.touches[0].phase == TouchPhase.Canceled || Input.touches[0].phase == TouchPhase.Ended)
            {
                if (selectedTransform != null)
                    MyOnMouseUp(selectedTransform);
                isDrag = false;
            }
        }

        #endregion

        if (isDrag && selectedTransform != null)
        {
            selectedTransform.transform.position = GetMouseWorlPos() + mOffset;
        }
    }

    private void MyOnMouseDown(Transform myObject)
    {
        myObject.transform.position += holdOffset;

        mZCoord = targetCam.WorldToScreenPoint(myObject.transform.position).z;

        mOffset = myObject.transform.position - GetMouseWorlPos();
    }

    private void MyOnMouseUp(Transform myObject)
    {
        // myObject.OnDragEnd();

        bool isFit = false;

        MeshRenderer meshRenderer = selectedTransform.GetComponent<MeshRenderer>();
        Bounds bounds = meshRenderer.bounds;

        Ray ray = new Ray(bounds.center, Vector3.forward);
        if (Physics.Raycast(ray, out RaycastHit hit))
        {
            if (hit.transform.TryGetComponent(out Shelf shelf))
            {
                if (selectedProduct.Type == shelf.Type)
                {
                    Transform closestTransform = shelf.GetClosestEmptyPoint(hit.point);
                    if (closestTransform)
                    {
                        isFit = true;
                        selectedTransform.DOMove(closestTransform.position, 0.5f);
                        selectedTransform.SetParent(null);
                    }
                }
            }
        }

        if (!isFit)
            selectedTransform.DOMove(selectedProductFirstPosition, 0.5f);
        selectedTransform = null;
    }

    public void DeleteSelectedObject()
    {
    }


    Vector3 GetMouseWorlPos()
    {
        Vector3 mousePoint = Input.mousePosition;
        mousePoint.z = mZCoord;
        return targetCam.ScreenToWorldPoint(mousePoint);
    }
}