﻿using UnityEngine;

public struct MyProduct
{
    private Transform productTransform;
    private ProductType type;

    public MyProduct(Transform productTransform, ProductType type)
    {
        this.productTransform = productTransform;
        this.type = type;
    }

    public Transform ProductTransform
    {
        get => productTransform;
    }

    public ProductType Type
    {
        get => type;
    }
}

public enum ProductType
{
    Drink,
    Milk,
    Jam,
    Juice
}