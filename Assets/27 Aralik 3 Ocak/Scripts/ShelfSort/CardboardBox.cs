﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardboardBox : MonoBehaviour
{
    private List<Transform> products;

    [SerializeField] private
        ProductType currentProductType;

    // Start is called before the first frame update
    void Start()
    {
        Transform itemHolder = transform.Find("ItemHolder");

        products = new List<Transform>();

        for (int i = 0; i < itemHolder.childCount; i++)
        {
            products.Add(itemHolder.GetChild(i));
        }
    }

    public MyProduct GetProduct(Vector3 point)
    {
        float tempDistance = Mathf.Infinity;
        Transform closestProduct = null;
        for (int i = 0; i < products.Count; i++)
        {
            float distance = Vector3.Distance(point, products[i].transform.position);
            if (distance < tempDistance)
            {
                tempDistance = distance;
                closestProduct = products[i];
            }
        }

        MyProduct productStruct = new MyProduct(closestProduct, currentProductType);

        return productStruct;
    }
}
