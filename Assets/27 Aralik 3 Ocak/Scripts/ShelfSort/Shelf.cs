﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shelf : MonoBehaviour
{
    [SerializeField] private ProductType productType;

    private List<Transform> deActiveProducts;
    private List<Transform> activeProducts;

    public ProductType Type
    {
        get => productType;
    }

    // Start is called before the first frame update
    void Start()
    {
        Transform itemHolder = transform.Find("Products");

        activeProducts = new List<Transform>();
        deActiveProducts = new List<Transform>();

        for (int i = 0; i < itemHolder.childCount; i++)
        {
            if (Random.value < 0.5f)
            {
                itemHolder.GetChild(i).gameObject.SetActive(false);
                deActiveProducts.Add(itemHolder.GetChild(i));
            }
            else
            {
                itemHolder.GetChild(i).gameObject.SetActive(true);
                activeProducts.Add(itemHolder.GetChild(i));
            }
        }
    }

    public Transform GetClosestEmptyPoint(Vector3 point)
    {
        float tempDistance = Mathf.Infinity;
        Transform closestProduct = null;
        for (int i = 0; i < deActiveProducts.Count; i++)
        {
            float distance = Vector3.Distance(point, deActiveProducts[i].transform.position);
            if (distance < tempDistance)
            {
                tempDistance = distance;
                closestProduct = deActiveProducts[i];
            }
        }
        deActiveProducts.Remove(closestProduct);
        return closestProduct;
    }
}