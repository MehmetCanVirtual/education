﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    private bool isPressed = false;

    private Vector2 startPosition;
    private Vector2 deltaPosition;


    [SerializeField] private float treshold = 1;
    public Vector2 JoystickValue;

    private float timer = 0;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            isPressed = true;
            startPosition = Input.mousePosition;
        }

        if (Input.GetMouseButtonUp(0))
        {
            isPressed = false;
            JoystickValue = Vector2.zero;
        }

        if (isPressed)
        {
            deltaPosition = (Vector2) Input.mousePosition - startPosition;
            // Debug.Log(deltaPosition);

            Vector2 calculatedDeltaPosition = deltaPosition;

            // if (calculatedDeltaPosition.x > 1)
            //     calculatedDeltaPosition.x = 1;
            //
            // if (calculatedDeltaPosition.y > 1)
            //     calculatedDeltaPosition.y = 1;
            //
            // if (calculatedDeltaPosition.x < -1)
            //     calculatedDeltaPosition.x = -1;
            //
            // if (calculatedDeltaPosition.y < -1)
            //     calculatedDeltaPosition.y = -1;

            calculatedDeltaPosition.x *= 0.1f * treshold;
            calculatedDeltaPosition.y *= 0.05f * treshold;

            calculatedDeltaPosition.x = Mathf.Clamp(calculatedDeltaPosition.x, -1, 1);
            calculatedDeltaPosition.y = Mathf.Clamp(calculatedDeltaPosition.y, -1, 1);

            //  Debug.Log(calculatedDeltaPosition);

            JoystickValue = calculatedDeltaPosition;

            // timer += Time.deltaTime;
            // float value = Mathf.Lerp(-10, 10, timer);
            // Debug.Log(value);
        }
    }
}