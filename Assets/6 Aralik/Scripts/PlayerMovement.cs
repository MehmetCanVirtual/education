﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private InputManager inputManager;
    private Animator animator;
    [SerializeField] private float movementSpeed = 1;
    [SerializeField] private float angularSpeed = 1;
    private Camera mainCam;

    private float currentSpeed;

    private void Start()
    {
        animator = GetComponentInChildren<Animator>();
        mainCam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        currentSpeed = 0;

        if (inputManager.JoystickValue != Vector2.zero)
        {
            // Debug.Log(inputManager.JoystickValue);

            Vector3 position = transform.position;

            position.x += inputManager.JoystickValue.x * movementSpeed * Time.deltaTime;
            position.z += inputManager.JoystickValue.y * movementSpeed * Time.deltaTime;


            transform.position = position;


            Vector3 direction = new Vector3(inputManager.JoystickValue.x, 0, inputManager.JoystickValue.y);

            //  transform.forward = direction;

            transform.forward = Vector3.Lerp(transform.forward, direction, Time.deltaTime * angularSpeed);

            currentSpeed = inputManager.JoystickValue.magnitude;

            //transform.position += transform.forward * (inputManager.JoystickValue.y * speed * Time.deltaTime);

            // transform. += inputManager.JoystickValue.x * angularSpeed * Vector3.up;

            // transform.position += transform.forward * (inputManager.JoystickValue.y * speed * Time.deltaTime);
        }

       // Debug.Log("currentSpeed " + currentSpeed);

        animator.SetFloat("speed", currentSpeed);
    }
}