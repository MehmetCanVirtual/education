﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barrel : MonoBehaviour
{
    private Transform primaryModelHolder;

    private Transform secondaryModelHolder;

    private Rigidbody[] rigidbodies;

    // Start is called before the first frame update
    void Start()
    {
        primaryModelHolder = transform.Find("PrimaryModelHolder");
        secondaryModelHolder = transform.Find("SecondaryModelHolder");

        rigidbodies = new Rigidbody[secondaryModelHolder.childCount];

        for (int i = 0; i < secondaryModelHolder.childCount; i++)
        {
            rigidbodies[i] = secondaryModelHolder.GetChild(i).GetComponent<Rigidbody>();
            
        }
    }

    public void Patlat(Vector3 direction, float force)
    {
        primaryModelHolder.gameObject.SetActive(false);
        secondaryModelHolder.gameObject.SetActive(true);

        for (int i = 0; i < rigidbodies.Length; i++)
        {
            rigidbodies[i].AddForce(direction * force, ForceMode.Impulse);
        }
    }
}