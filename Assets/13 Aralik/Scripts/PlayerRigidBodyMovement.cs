﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRigidBodyMovement : MonoBehaviour
{
    private Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Debug.Log("StartSpeed " + rb.velocity);
    }
    
    // Update is called once per frame
    void Update()
    {
        rb.velocity = Vector3.forward;
        Debug.Log("CurrentVelocity " + rb.velocity);
        Debug.Log("CurrentSpeed " + rb.velocity.magnitude);
    }
}