﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerColliderController : MonoBehaviour
{
    private bool isCollisionOtherObject = false;

    private void OnCollisionEnter(Collision other)
    {
        if (other.transform.TryGetComponent(out Obstacle obstacle))
        {
            Debug.Log("Enter Obstace" + other.collider.name);
        }

        if (other.transform.TryGetComponent(out Barrel barrel))
        {
            Debug.Log("Enter Barrel" + other.collider.name);
            barrel.Patlat(other.GetContact(0).normal * -1, 5);
        }
    }

    private void OnCollisionExit(Collision other)
    {
        if (other.transform.TryGetComponent(out Obstacle obstacle))
        {
            Debug.Log("Exit Obstacle" + other.collider.name);
        }

        if (other.transform.TryGetComponent(out Barrel barrel))
        {
            Debug.Log("Exit Barrel" + other.collider.name);
        }
    }
}