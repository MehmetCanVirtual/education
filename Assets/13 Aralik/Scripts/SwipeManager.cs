﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeManager : MonoBehaviour
{
    public SwipeDireciton swipeDireciton = SwipeDireciton.None;

    public bool isSwipeRight;
    public bool isSwipeLeft;
    public bool isSwipeUp;
    public bool isSwipeDown;


    private Vector2 mouseStartPosition;

    public float magnitute = 10;

    private bool isPressed;

    // Update is called once per frame
    void Update()
    {
        isSwipeDown = isSwipeLeft = isSwipeRight = isSwipeUp = false;
        swipeDireciton = SwipeDireciton.None;

        if (Input.GetMouseButtonDown(0))
        {
            mouseStartPosition = Input.mousePosition;
            isPressed = true;
        }

        if (Input.GetMouseButtonUp(0))
        {
            isPressed = false;

            Vector2 deltaPosition = (Vector2) Input.mousePosition - mouseStartPosition;

            if (deltaPosition.magnitude > magnitute)
            {
                if (deltaPosition.y > deltaPosition.x)
                {
                    if (Mathf.Abs(deltaPosition.x) > Mathf.Abs(deltaPosition.y))
                    {
                        Debug.Log("left");
                        isSwipeLeft = true;
                        swipeDireciton = SwipeDireciton.Left;
                    }
                    else
                    {
                        Debug.Log("up");
                        isSwipeUp = true;
                        swipeDireciton = SwipeDireciton.Up;
                    }
                }
                else
                {
                    if (Mathf.Abs(deltaPosition.x) > Mathf.Abs(deltaPosition.y))
                    {
                        Debug.Log("right");
                        isSwipeRight = true;
                        swipeDireciton = SwipeDireciton.Right;
                    }
                    else
                    {
                        Debug.Log("down");
                        isSwipeDown = true;
                        swipeDireciton = SwipeDireciton.Down;
                    }
                }
            }
        }
    }

    public enum SwipeDireciton
    {
        None,
        Right,
        Left,
        Up,
        Down
    }
}