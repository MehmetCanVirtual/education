﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunnerCharacter : MonoBehaviour
{
    [SerializeField] private float speed = 100;

    public SwipeManager swipeManager;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        var targetPosition = transform.position;


        if (swipeManager.swipeDireciton == SwipeManager.SwipeDireciton.Right)
        {
            targetPosition += transform.right * 2;
        }
        else if (swipeManager.swipeDireciton == SwipeManager.SwipeDireciton.Left)
        {
            targetPosition += transform.right * -2;
        }
        else if (swipeManager.swipeDireciton == SwipeManager.SwipeDireciton.Up)
        {
            targetPosition += transform.up * 2;
        }
        else if (swipeManager.swipeDireciton == SwipeManager.SwipeDireciton.Down)
        {
            targetPosition += transform.up * -2;
        }
        //
        // if (swipeManager.isSwipeRight)
        // {
        //     targetPosition += transform.right * 2;
        // }
        // else if (swipeManager.isSwipeLeft)
        // {
        //     targetPosition += transform.right * -2;
        // }
        // else if (swipeManager.isSwipeDown)
        // {
        //     targetPosition += transform.up * -2;
        // }
        // else if (swipeManager.isSwipeUp)
        // {
        //     targetPosition += transform.up * 2;
        // }

        targetPosition += transform.forward * (speed * Time.deltaTime);

        transform.position = targetPosition;
    }
}