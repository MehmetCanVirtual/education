﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class PlayerMovementJoystick : MonoBehaviour
{
    [SerializeField] private VariableJoystick joystick;
    private Animator animator;
    private Rigidbody rb;
    [SerializeField] private float movementSpeed = 1;
    [SerializeField] private float angularSpeed = 1;
    private Camera mainCam;

    private float currentSpeed;


    private void Start()
    {
        animator = GetComponentInChildren<Animator>();
        mainCam = Camera.main;
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        currentSpeed = 0;

        //Debug.Log("Joystick Horizontal " + joystick.Horizontal + "Joystick Vertical " + joystick.Vertical);

        if (joystick.Horizontal != 0 && joystick.Vertical != 0)
        {
            // Debug.Log(inputManager.JoystickValue);

            Vector3 calculatedVelocity = rb.velocity;

            calculatedVelocity.x = joystick.Horizontal * movementSpeed * Time.deltaTime;
            calculatedVelocity.z = joystick.Vertical * movementSpeed * Time.deltaTime;

            Debug.Log("calculatedVelocity " + calculatedVelocity);

            rb.velocity = calculatedVelocity;

            Vector3 direction = new Vector3(joystick.Horizontal, 0, joystick.Vertical);

            //  transform.forward = direction;

            transform.rotation=Quaternion.LookRotation(direction);
            
            
           // transform.forward = Vector3.Lerp(transform.forward, direction, Time.deltaTime * angularSpeed);

            currentSpeed = 100;

            //transform.position += transform.forward * (inputManager.JoystickValue.y * speed * Time.deltaTime);

            // transform. += inputManager.JoystickValue.x * angularSpeed * Vector3.up;

            // transform.position += transform.forward * (inputManager.JoystickValue.y * speed * Time.deltaTime);
        }
        else
        {
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
        }


        // Debug.Log("currentSpeed " + currentSpeed);

        animator.SetFloat("speed", currentSpeed);
    }
}