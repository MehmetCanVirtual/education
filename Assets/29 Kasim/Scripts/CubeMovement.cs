﻿using UnityEngine;

public class CubeMovement : MonoBehaviour
{
    private Transform thisTransform;

    // private Vector3 direction;
    [SerializeField] private float speed = 0.01f;

    // Start is called before the first frame update
    void Start()
    {
        thisTransform = GetComponent<Transform>();
        //   direction = thisTransform.forward * speed;
        
        // Vector3 A = new Vector3(1, 2, 1);
        // Vector3 D = A * 3f;
        Debug.Log(thisTransform.position);
    }

    // Update is called once per frame
    void Update()
    {
        thisTransform.position += thisTransform.forward * speed;
    }
}