﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeLogger : MonoBehaviour
{
    // [HideInInspector] public int logCount = 2;
    //
     [SerializeField] private float logShowTime = 2.5f;

    private int currentLogCount;

    public void WriteConsole(string otherObjectName)
    {
        Debug.Log("Called By " + otherObjectName + "  " + name, transform);
        UpdateLogCount();
    }

    private void UpdateLogCount()
    {
        currentLogCount++;
    }

    //Awake is called before the scene load
    private void Awake()
    {
        //WriteConsole(logText);
    }

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Start called " + name);
        UpdateLogCount();
    }
}